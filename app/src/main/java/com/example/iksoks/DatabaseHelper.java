package com.example.iksoks;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import androidx.annotation.Nullable;

import java.lang.annotation.Target;
import java.lang.reflect.Array;
import java.util.ArrayList;

public class DatabaseHelper extends SQLiteOpenHelper {
    private static final String TAG = "DatabaseHelper";

    private static final String TABLE_NAME = "HistoryTable";
    private static final String COL1 = "ID";
    private static final String COL2 = "Player1";
    private static final String COL3 = "Player2";
    private static final String COL4 = "Result";

    public DatabaseHelper(Context context) {
        super(context, TABLE_NAME, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String createTable = "CREATE TABLE " +
                TABLE_NAME +
                "(" +
                COL1 + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                COL2 + " TEXT," +
                COL3 + " TEXT," +
                COL4 + " TEXT" +
                ")";
        db.execSQL(createTable);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        onCreate(db);

    }

    public boolean addData(String player1, String player2, String result){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(COL2, player1);
        contentValues.put(COL3, player2);
        contentValues.put(COL4, result);
        Log.d(TAG, "addData: Adding " + player1 + player2 + result + " to" + TABLE_NAME);
        long outcome = db.insert(TABLE_NAME, null, contentValues);

         if(outcome == -1){
             return false;
         }else{
             return true;
         }
    }

    public ArrayList<Results> getData(){
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursorResults = db.rawQuery("SELECT * FROM " + TABLE_NAME,null);

        ArrayList<Results> resultsList= new ArrayList<>();
        while(cursorResults.moveToNext()){
            resultsList.add(new Results(cursorResults.getString(1),
                    cursorResults.getString(2),
                    cursorResults.getString(3)));
        }



        return resultsList;
    }



}
