package com.example.iksoks;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class GameDisplay extends AppCompatActivity {

    private TicTacToeBoard ticTacToeBoard;
    DatabaseHelper mDatabaseHelper;
    String player1;
    String player2;
    String winner;
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.game_display);
        mDatabaseHelper = new DatabaseHelper(this);
        Button playAgainBTN=findViewById(R.id.play_again);
        Button homeBTN=findViewById(R.id.home_button);
        TextView playerTurn = findViewById(R.id.player_display);

        playAgainBTN.setVisibility(View.GONE);
        homeBTN.setVisibility(View.GONE);

        String[] playerNames = getIntent().getStringArrayExtra("PLAYER_NAMES");

        if (playerNames != null){
            playerTurn.setText((playerNames[0]+"'s Turn"));
        }

        ticTacToeBoard = findViewById(R.id.ticTacToeBoard);

        ticTacToeBoard.setUpGame(playAgainBTN,homeBTN,playerTurn,playerNames);
    }




    public void playAgainButtonClick(View view){
        ticTacToeBoard.resetGame();
        ticTacToeBoard.invalidate();
        String[] players=ticTacToeBoard.game.getPlayerNames();
        player1=players[0];
        player2=players[1];
        winner=ticTacToeBoard.game.getWinner();
        AddData(player1,player2,winner);
    }

    public void homeButtonClick(View view){
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        String[] players=ticTacToeBoard.game.getPlayerNames();
        player1=players[0];
        player2=players[1];
        winner=ticTacToeBoard.game.getWinner();
        AddData(player1,player2,winner);
    }

    public void AddData(String player1, String player2, String result){
        boolean insertData = mDatabaseHelper.addData(player1,player2,result);

    }
}