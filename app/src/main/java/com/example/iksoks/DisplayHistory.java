package com.example.iksoks;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.view.View;

import java.util.ArrayList;

public class DisplayHistory extends AppCompatActivity {


    private ArrayList<Results> resultsList;
    private DatabaseHelper dbHandler;
    private ResultsRVAdapter resultsRVAdapter;
    private RecyclerView resultsRV;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.display_history);


        resultsList = new ArrayList<>();
        dbHandler = new DatabaseHelper(this);


        resultsList = dbHandler.getData();


        resultsRVAdapter = new ResultsRVAdapter(resultsList, this);
        resultsRV = findViewById(R.id.RVResults);


        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, RecyclerView.VERTICAL, false);
        resultsRV.setLayoutManager(linearLayoutManager);


        resultsRV.setAdapter(resultsRVAdapter);
    }
    public void homeButtonClick(View view){
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }
}