package com.example.iksoks;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class ResultsRVAdapter extends RecyclerView.Adapter<ResultsRVAdapter.ViewHolder> {

    private ArrayList<Results> resultsList;
    private Context context;

    // constructor
    public ResultsRVAdapter(ArrayList<Results> resultsList, Context context) {
        this.resultsList = resultsList;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.results_rv_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        Results modal = resultsList.get(position);
        holder.player1TV.setText(modal.getPlayer1());
        holder.player2TV.setText(modal.getPlayer2());
        holder.winnerTV.setText(modal.getWinner());

    }

    @Override
    public int getItemCount() {
        // returning the size of our array list
        return resultsList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        // creating variables for our text views.
        private TextView player1TV,player2TV,winnerTV;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            // initializing our text views
            player1TV = itemView.findViewById(R.id.TVplayer1);
            player2TV = itemView.findViewById(R.id.TVplayer2);
            winnerTV = itemView.findViewById(R.id.TVwinner);

        }
    }


}
